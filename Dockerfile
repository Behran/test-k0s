FROM golang:1.19 AS builder

WORKDIR /go/src/app/
COPY src .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app cmd/k0s/main.go && chmod +x app

FROM alpine:3.16.2

WORKDIR /usr/local/bin

COPY --from=builder /go/src/app/app app

ENTRYPOINT ["/usr/local/bin/app"]

EXPOSE 80